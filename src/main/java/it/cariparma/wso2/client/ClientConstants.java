
package it.cariparma.wso2.client;

public class ClientConstants {

    public static final String ACCESS_USERNAME = "accessUsername";

    public static final String CAPTCHA_DISABLE = "captchaDisable";

    public static final String ACCESS_PASSWORD = "accessPassword";

    public static final String TRUSTSTORE_PATH = "trustStorePath";

    public static final String TRUSTSTORE_PROPERTY = "javax.net.ssl.trustStore";
}
