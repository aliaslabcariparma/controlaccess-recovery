
package it.cariparma.wso2.controller;

import it.cariparma.wso2.client.ClientConstants;
import it.cariparma.wso2.client.authenticator.ServiceAuthenticator;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class IndexController extends HttpServlet {

	private static final long serialVersionUID = 4666829873705984434L;
	
	private Log log = LogFactory.getLog(IndexController.class);

	public void init(ServletConfig config) throws ServletException {

		super.init(config);

		ServletContext ctx = config.getServletContext();
		ServiceAuthenticator sAuthenticator = ServiceAuthenticator.getInstance();
		sAuthenticator.setAccessUsername(ctx.getInitParameter(ClientConstants.ACCESS_USERNAME));
		sAuthenticator.setAccessPassword(ctx.getInitParameter(ClientConstants.ACCESS_PASSWORD));

		String trustStorePath = ctx.getInitParameter(ClientConstants.TRUSTSTORE_PATH);
		System.setProperty(ClientConstants.TRUSTSTORE_PROPERTY,
				Thread.currentThread().getContextClassLoader().getResource(trustStorePath).getPath());

	}

	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {

		this.doPost(req, res);

	}

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {

		String parameter = req.getParameter("action");
		
		if(parameter == null){
			req.setAttribute("action", "reset");
		}else{
			
			switch (parameter.toUpperCase()) {
			case "RESET":
				req.setAttribute("action", "reset");
				break;
			case "SET":
				req.setAttribute("action", "setting");
				break;
			default:
				req.setAttribute("action", "reset");
				break;
			}
		}
		
		String destinationUri = "/resetPassword";

		RequestDispatcher view = null;

		HttpSession session = req.getSession(true);

		if (session != null) {
			view = req.getRequestDispatcher(destinationUri);
			view.forward(req, res);

		}
	}
}
