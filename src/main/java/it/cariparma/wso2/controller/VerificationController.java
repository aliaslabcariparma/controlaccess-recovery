
package it.cariparma.wso2.controller;

import it.cariparma.wso2.client.UserInformationRecoveryClient;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.axis2.context.ConfigurationContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.wso2.carbon.CarbonConstants;
import org.wso2.carbon.captcha.mgt.beans.xsd.CaptchaInfoBean;
import org.wso2.carbon.identity.mgt.stub.beans.VerificationBean;

public class VerificationController extends HttpServlet {

	private static final long serialVersionUID = 7176687492831673742L;
	
	private Log log = LogFactory.getLog(VerificationController.class);
	
	UserInformationRecoveryClient client;

    public void init() {
        try {
            ConfigurationContext configContext = (ConfigurationContext) this.getServletContext().getAttribute(CarbonConstants.CONFIGURATION_CONTEXT);
            String carbonServerUrl = this.getServletConfig().getServletContext().getInitParameter("carbonServerUrl");
            client = new UserInformationRecoveryClient(carbonServerUrl, configContext);
        } catch (Exception e) {
           log.error(e.getMessage(), e);
        }
    }

    public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException,
            IOException {

        HttpSession session = req.getSession(false);
        String confirmation = (String) session.getAttribute("confirmation");
        String viewPage = null;
        String username = req.getParameter("username");
        String captchaPath = req.getParameter("captchaImagePath");
        String captchaKey = req.getParameter("captchaSecretKey");
        String captchaAnswer = req.getParameter("captchaAnswer");
    	String callBackUrl = this.getServletConfig().getServletContext().getInitParameter("callBackUrl");
		req.setAttribute("callBackUrl", callBackUrl);
        if (confirmation != null && session != null) {

            // verify conf code
            CaptchaInfoBean captchaInfoBean = new CaptchaInfoBean();
            captchaInfoBean.setImagePath(captchaPath);
            captchaInfoBean.setSecretKey(captchaKey);
            captchaInfoBean.setUserAnswer(captchaAnswer);
            VerificationBean verificationBean = null;

            verificationBean = client.verifyConfirmationCode(username, confirmation, captchaInfoBean);

            if (verificationBean != null && verificationBean.getVerified()) {

                session.setAttribute("confirmation", verificationBean.getKey());
                session.setAttribute("username", username);
                viewPage = "password_reset.jsp";

            } else {
                req.setAttribute(
                        "errors",
                        "Le informazioni inserite non sono valide. La Username o il codice captcha non sono corretti. Impossibile procedere con la richiesta.");
                viewPage = "error.jsp";
            }

        } else {
        	
            req.setAttribute("errors",
            		"Confirmation code assente o session  invalida. Impossibile Procedere.");
            viewPage = "error.jsp";
        }

        RequestDispatcher view = req.getRequestDispatcher(viewPage);
        view.forward(req, res);

    }

    public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

        doPost(req, res);
    }
}
