
package it.cariparma.wso2.controller;

import it.cariparma.wso2.client.UserInformationRecoveryClient;
import it.cariparma.wso2.utils.CaptchaUtils;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.axis2.context.ConfigurationContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.wso2.carbon.CarbonConstants;
import org.wso2.carbon.captcha.mgt.beans.xsd.CaptchaInfoBean;

public class UserInfoViewController extends HttpServlet {

	private static final long serialVersionUID = -3061371652180535802L;
	
	private static final  Logger log = LoggerFactory.getLogger(UserInfoViewController.class);

	private UserInformationRecoveryClient client;

	public void init() {
		try {
			ConfigurationContext configContext = (ConfigurationContext) this.getServletContext().getAttribute(CarbonConstants.CONFIGURATION_CONTEXT);
			String carbonServerUrl = this.getServletConfig().getServletContext().getInitParameter("carbonServerUrl");
			client = new UserInformationRecoveryClient(carbonServerUrl, configContext);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}

	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		this.doPost(req, res);
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

		HttpSession session = req.getSession(false);

		if (session != null) {

			CaptchaInfoBean captchaInfoBean;

			try {

				captchaInfoBean = client.generateCaptcha();

			} catch (Exception e) {
				return;
			}

			if (captchaInfoBean == null) {
				return;
			}


			String carbonServerUrl = this.getServletConfig().getServletContext().getInitParameter("carbonServerUrl");
			req.setAttribute("carbonServerUrl", carbonServerUrl);

			String captchaImagePath = captchaInfoBean.getImagePath();
			String captchaImageUrl = carbonServerUrl + captchaImagePath;
			String captchaSecretKey = captchaInfoBean.getSecretKey();
			
			File tmpFile = CaptchaUtils.copyFileFromURL(new URL(captchaImageUrl), captchaImagePath);
			String encodeCapchaBase64 = CaptchaUtils.encodeCapchaBase64(tmpFile);
			tmpFile.deleteOnExit();
			
			req.setAttribute("encodeCapchaBase64", encodeCapchaBase64);
			req.setAttribute("captchaImageUrl", captchaImageUrl);
			req.setAttribute("captchaSecretKey", captchaSecretKey);
			req.setAttribute("captchaImagePath", captchaImagePath);
			session.setAttribute("confirmation", captchaInfoBean.getSecretKey());
			req.setAttribute("recoveryMethod", "notification");
			String callBackUrl = this.getServletConfig().getServletContext().getInitParameter("callBackUrl");
			req.setAttribute("callBackUrl", callBackUrl);
			RequestDispatcher dispatcher;
			dispatcher = req.getRequestDispatcher("/recover_request_info.jsp");
			dispatcher.forward(req, res);
		}
	}
}
