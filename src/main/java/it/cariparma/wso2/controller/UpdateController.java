
package it.cariparma.wso2.controller;

import it.cariparma.wso2.client.UserInformationRecoveryClient;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.axis2.context.ConfigurationContext;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.wso2.carbon.CarbonConstants;
import org.wso2.carbon.identity.mgt.stub.beans.VerificationBean;

public class UpdateController extends HttpServlet {
	
	private static final long serialVersionUID = 494910795175776865L;

	private Log log = LogFactory.getLog(UpdateController.class);

	private UserInformationRecoveryClient client;

	public void init() {
		try {
			ConfigurationContext configContext =
					(ConfigurationContext) this.getServletContext().getAttribute(CarbonConstants.
							CONFIGURATION_CONTEXT);
			String carbonServerUrl = this.getServletConfig().getServletContext()
					.getInitParameter("carbonServerUrl");

			client = new UserInformationRecoveryClient(carbonServerUrl, configContext);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException,
	IOException {

		HttpSession session = req.getSession(false);

		String confirmation = (String) session.getAttribute("confirmation");
		String username = (String) session.getAttribute("username");
		String newPassword = req.getParameter("password");
		String confirmPassword = req.getParameter("confirmPassword");

		String callBackUrl = this.getServletConfig().getServletContext().getInitParameter("callBackUrl");
		req.setAttribute("callBackUrl", callBackUrl);
		
		String viewPage = null;

		if (confirmation != null && session != null) {

			if(StringUtils.isNotEmpty(newPassword) && newPassword.equals(confirmPassword)){
				// reset the password
				VerificationBean vBean = client.resetPassword(username, confirmation, newPassword);
				if (vBean != null && vBean.getVerified()) {
					req.setAttribute("validationStatus", vBean.getVerified());
					viewPage = "password_reset_result.jsp";
				} else {

					req.setAttribute("errors",
							"Missing confirmation code or invalid session. Cannot proceed.");
					viewPage = "error.jsp";
				}

			}else{
				req.setAttribute("errors", "Spiacenti le password inserite non corrispondono. Riprova");
				viewPage = "error.jsp";
			}
		} else {

			req.setAttribute("errors",
					"Missing confirmation code or invalid session. Cannot proceed.");
			viewPage = "error.jsp";
		}

		RequestDispatcher view = req.getRequestDispatcher(viewPage);
		view.forward(req, res);

	}

	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException,
	IOException {

		doPost(req, res);
	}
}
