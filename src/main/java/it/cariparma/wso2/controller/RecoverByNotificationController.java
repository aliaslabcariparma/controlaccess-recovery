
package it.cariparma.wso2.controller;

import it.cariparma.wso2.client.UserInformationRecoveryClient;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.axis2.context.ConfigurationContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.wso2.carbon.CarbonConstants;
import org.wso2.carbon.identity.mgt.stub.beans.VerificationBean;

public class RecoverByNotificationController extends HttpServlet {

   
	private static final long serialVersionUID = -1832013740478099838L;
	
	private Log log = LogFactory.getLog(RecoverByNotificationController.class);
	
	private UserInformationRecoveryClient client;

    public void init() {
        try {

            ConfigurationContext configContext = (ConfigurationContext) this.getServletContext().getAttribute(CarbonConstants.CONFIGURATION_CONTEXT);
            String carbonServerUrl = this.getServletConfig().getServletContext().getInitParameter("carbonServerUrl");
            client = new UserInformationRecoveryClient(carbonServerUrl, configContext);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException,
            ServletException {
        doPost(req, res);
    }

    public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException,
            ServletException {

        HttpSession session = req.getSession(false);
        String confirmation = (String) session.getAttribute("confirmation");
        String username = (String) session.getAttribute("username");
        VerificationBean vBean = null;
        String viewPage = null;

        if (session != null) {

            vBean = client.sendRecoveryNotification(username, confirmation, "EMAIL");

            if (vBean.getVerified()) {
                req.setAttribute("validationStatus", Boolean.valueOf(true));
                viewPage = "recover_status.jsp";
            } else {
            	String callBackUrl = this.getServletConfig().getServletContext().getInitParameter("callBackUrl");
    			req.setAttribute("callBackUrl", callBackUrl);
                req.setAttribute("validationStatus", Boolean.valueOf(false));
                viewPage = "error.jsp";
            }
        }

        RequestDispatcher view = req.getRequestDispatcher(viewPage);
        view.forward(req, res);
    }

}
