package it.cariparma.wso2.controller;

import it.cariparma.wso2.client.UserInformationRecoveryClient;
import it.cariparma.wso2.utils.CaptchaUtils;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.axis2.context.ConfigurationContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.wso2.carbon.CarbonConstants;
import org.wso2.carbon.captcha.mgt.beans.xsd.CaptchaInfoBean;

public class ConfirmationController extends HttpServlet {

	private static final long serialVersionUID = 1186980726024117792L;
	
	private Log log = LogFactory.getLog(ConfirmationController.class);

	private UserInformationRecoveryClient client;

	@Override
	public void init() throws ServletException {
		try {
			ConfigurationContext configContext = (ConfigurationContext) this.getServletContext().getAttribute(CarbonConstants.CONFIGURATION_CONTEXT);
			String carbonServerUrl = this.getServletConfig().getServletContext().getInitParameter("carbonServerUrl");
			client = new UserInformationRecoveryClient(carbonServerUrl, configContext);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}

	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		this.doPost(req, res);
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		
		if(log.isDebugEnabled()){
			log.debug("doPost ");
		}
		HttpSession session = req.getSession(false);

		if (session != null) {

			CaptchaInfoBean captchaInfoBean;

			try {

				captchaInfoBean = client.generateCaptcha();

			} catch (Exception e) {
				log.error(e.getMessage(), e);
				return;
			}

			if (captchaInfoBean == null) {
				if(log.isDebugEnabled()){
					log.debug("captchaInfoBean == null");
				}
				return;
			}

			String carbonServerUrl = this.getServletConfig().getServletContext() .getInitParameter("carbonServerUrl");

			String captchaImagePath = captchaInfoBean.getImagePath();
			String captchaImageUrl = carbonServerUrl + captchaImagePath;
			String captchaSecretKey = captchaInfoBean.getSecretKey();
			File tmpFile = CaptchaUtils.copyFileFromURL(new URL(captchaImageUrl), captchaImagePath);
			String encodeCapchaBase64 = CaptchaUtils.encodeCapchaBase64(tmpFile);
			tmpFile.deleteOnExit();
			
			req.setAttribute("encodeCapchaBase64", encodeCapchaBase64);
			req.setAttribute("captchaImageUrl", captchaImageUrl);
			req.setAttribute("captchaSecretKey", captchaSecretKey);
			req.setAttribute("captchaImagePath", captchaImagePath);
			session.setAttribute("confirmation", req.getParameter("confirmation"));
			RequestDispatcher dispatcher;
			dispatcher = req.getRequestDispatcher("/recover_confirmation_info.jsp");
			dispatcher.forward(req, res);
		}

	}

}
