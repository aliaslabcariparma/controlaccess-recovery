package it.cariparma.wso2.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CaptchaUtils {
	
	private static final  Logger LOGGER = LoggerFactory.getLogger(CaptchaUtils.class);

	public static String encodeCapchaBase64(File captcha){
		byte[] imgBytes = null;
		try {
			imgBytes = IOUtils.toByteArray(new FileInputStream(captcha));
			LOGGER.info("########### captcha absolute path: "+captcha.getAbsolutePath() +" - imgBytes: "+imgBytes);
		} catch (IOException e) {
			LOGGER.error(e.getMessage(), e);
		}
		byte[] imgBytesAsBase64 = Base64.encodeBase64(imgBytes);
		String imgDataAsBase64 = new String(imgBytesAsBase64);
		return "data:image/png;base64," + imgDataAsBase64;
	}
	
	public static File copyFileFromURL(URL url, String tmpFileName){
		File tmpFile = new File(FileUtils.getTempDirectoryPath()+"/"+tmpFileName);
		try {
			FileUtils.copyURLToFile(url, tmpFile);
		} catch (IOException e) {
			LOGGER.error(e.getMessage(), e);
		}
		return tmpFile;
	}
	
}
