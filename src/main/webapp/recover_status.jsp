<!DOCTYPE html>
<%@ page contentType="text/html; charset=iso-8859-1" language="java"%>

<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>User Recover</title>

<link rel="icon" href="./resources/images/favicon.ico" type="image/x-icon" />
<link href="./resources/css/bootstrap.min.css" rel="stylesheet">
<link href="./resources/css/Roboto.css" rel="stylesheet">
<link href="./resources/css/custom-common.css" rel="stylesheet">
<link href="./resources/css/cariparma.css" rel="stylesheet">
<link href="./resources/css/login.css" rel="stylesheet">

</head>

<body>

	<!-- header -->
	<div class="navbar navbar-inverse navbar-static-top cariparma-header">
		<div class="container">
			<a href="#" class="navbar-brand"> <img src="./resources/images/ca_logo.svg"
				alt="Control Access Module" title="Control Access Module"
				class="logo" style="height: 25px;">
			</a>
		</div>
	</div>

	<!-- page content -->
	<div class="container-fluid body-wrapper">

		<div class="row">
			<!-- content -->
			<div class="container col-xs-10 col-sm-6 col-md-6 col-lg-3 col-centered wr-content wr-login col-centered">
					<%
						Boolean status = (Boolean) request.getAttribute("validationStatus");
						if (status) {
					%>
					<div class="alert alert-success">
						<span class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span> E' stata inviata una email al suo indirizzo con i dettagli per reimpostare la password.
					</div>
					<div class="alert alert-warning">
						<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span><strong> Attenzione!!! </strong> La invitiamo a non chiudere questa pagina fino al completamento della procedura di reimpostazione della password.
					</div>
					<%
						} else {
					%>
	
					<div class="alert alert-danger">
					 	<strong><span class="glyphicon glyphicon-ban-circle" aria-hidden="true"></span> Spiacenti non � stato possibile
							verificare la username o il captcha.</strong>
					</div>
					<%
						}
					%>
			</div>
		</div>
		<!-- /content/body -->

	</div>

	<!-- footer -->
	<footer class="navbar navbar-default navbar-fixed-bottom cariparma-footer">
		<div class="container-fluid"></div>
	</footer>

</body>
</html>

