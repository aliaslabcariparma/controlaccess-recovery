<%@ page contentType="text/html; charset=iso-8859-1" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	String callBackUrl = (String) request.getAttribute("callBackUrl");
%>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>User Recover</title>

<link rel="icon" href="./resources/images/favicon.ico" type="image/x-icon" />
<link href="./resources/css/bootstrap.min.css" rel="stylesheet">
<link href="./resources/css/Roboto.css" rel="stylesheet">
<link href="./resources/css/custom-common.css" rel="stylesheet">
<link href="./resources/css/cariparma.css" rel="stylesheet">
<link href="./resources/css/login.css" rel="stylesheet">

</head>

<body>
	<div class="navbar navbar-inverse navbar-static-top cariparma-header">
		<div class="container">
			 <a href="home" class="navbar-brand"> <img src="<c:url value="/resources/images/ca_logo.svg" />" alt="Control Access Module" title="Control Access Module" class="logo" style="height: 25px;"> </a>
		</div>
	</div>
	<div class="container">
		<blockquote>
			<h1 style="font-size: 3em; color: #95C120;">404 - WOOPS!!!</h1>
			<p style="font-size: 1em;">Spiacenti pagina non trovata</p>
		</blockquote>
	</div>
	<div class="navbar navbar-default navbar-fixed-bottom cariparma-footer">
    <div class="container">
       
    </div>
</div>
</body>
</html>



