<%@ page contentType="text/html; charset=iso-8859-1" language="java"%>
<%
	String callBackUrl = (String) request.getAttribute("callBackUrl");
%>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>User Recover Error</title>

<link rel="icon" href="./resources/images/favicon.ico" type="image/x-icon" />
<link href="./resources/css/bootstrap.min.css" rel="stylesheet">
<link href="./resources/css/Roboto.css" rel="stylesheet">
<link href="./resources/css/custom-common.css" rel="stylesheet">
<link href="./resources/css/cariparma.css" rel="stylesheet">
<link href="./resources/css/login.css" rel="stylesheet">

</head>

<body>

	<!-- header -->
	<div class="navbar navbar-inverse navbar-static-top cariparma-header">
		<div class="container">
			<a href="#" class="navbar-brand"> <img src="./resources/images/ca_logo.svg"
				alt="Control Access Module" title="Control Access Module"
				class="logo" style="height: 25px;">
			</a>
		</div>
	</div>

	<!-- page content -->
	<div class="container-fluid body-wrapper">
		<div class="row">
			<!-- content -->
			<div
				class="container col-xs-10 col-sm-6 col-md-6 col-lg-3 col-centered wr-content wr-login col-centered">
				<div class="alert alert-danger">
					 	<span class="glyphicon glyphicon-ban-circle" aria-hidden="true"></span> <strong>Errore!!! </strong>
					<%=request.getAttribute("errors")%>
					</div>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
					<button id="back" class="col-xs-12 col-md-12 col-lg-12  btn cariparma-btn" type="button" onclick="javascript:location.href='<%=callBackUrl%>'">
						<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>
						Indietro
					</button>
				</div>
			</div>
		</div>
		<!-- /content/body -->
	</div>

	<!-- footer -->
	<footer class="navbar navbar-default navbar-fixed-bottom cariparma-footer">
		<div class="container-fluid"></div>
	</footer>

</body>
</html>

