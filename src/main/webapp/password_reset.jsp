<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Reset Password</title>

<link rel="icon" href="./resources/images/favicon.ico" type="image/x-icon" />
<link href="./resources/css/bootstrap.min.css" rel="stylesheet">
<link href="./resources/css/Roboto.css" rel="stylesheet">
<link href="./resources/css/custom-common.css" rel="stylesheet">
<link href="./resources/css/cariparma.css" rel="stylesheet">
<link href="./resources/css/login.css" rel="stylesheet">
<link href="./resources/css/messenger.css" rel="stylesheet">
<link href="./resources/css/messenger-theme-air.css" rel="stylesheet">
</head>

<body>

	<!-- header -->
	<div class="navbar navbar-inverse navbar-static-top cariparma-header">
		<div class="container">
			<a href="#" class="navbar-brand"> <img src="./resources/images/ca_logo.svg"
				alt="Control Access Module" title="Control Access Module"
				class="logo" style="height: 25px;">
			</a>
		</div>
	</div>

	<!-- page content -->
	<div class="container-fluid body-wrapper">

		<div class="row">
			<!-- content -->

			<div class="container col-xs-10 col-sm-6 col-md-6 col-lg-3 col-centered wr-content wr-login col-centered">
				<form method="POST" action='./update' id="resetPasswordForm">
					<h4 class="wr-title uppercase cariparma-bg padding-double white boarder-bottom-cariparma margin-none text-align_center">Nuova Password -  2/2</h4>
					<div class="clearfix"></div>
					<div class="boarder-all ">
					
						<div class="padding-double font-large text-align_center">Digita la nuova password</div>
						<!-- validation -->
						<div class="padding-double">
							
							<div id="alert" class="alert alert-danger" hidden>
								 <strong><span class="glyphicon glyphicon-ban-circle" aria-hidden="true"></span></strong> Le Password non corrispondono. Riprova
							</div>
							
							<div id="alertNoPolicy" class="alert alert-danger" hidden>
								 <strong><span class="glyphicon glyphicon-ban-circle" aria-hidden="true"></span></strong> La Password non rispetta i requisiti minimi richiesti. Riprova
							</div>

							<div id="alertPolicy" class="alert alert-info" hidden style="font-size:10px;">
								<strong><span class="glyphicon glyphicon-info-sign" aria-hidden="true"> </span></strong> La password deve contenere:
								<ul>
									<li>almeno 8 caratteri</li>
									<li>almeno una lettera maiuscola</li>
									<li>almeno una lettera minuscola</li>
									<li>almeno un numero</li>
									<li>almeno un carattere speciale tra questi !, @, #, $, %, &, * </li>
								</ul>
							</div>


							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group required" id="pwd-container">
								<div class="form-group">
									<label for="password" class="control-label">Nuova Password</label>
									<input type="password" class="form-control required usrName usrNameLength" id="password" name="password" required>
								</div>
								<div class="pwstrength_viewport_progress"></div>
								<div class="messages"></div>
								<div class="errors"></div>
							</div>
								<div
									class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group required">
									<label for="confirmPassword" class="control-label">Conferma Password</label> 
									<input id="confirmPassword" name="confirmPassword" type="password" class="form-control required usrName usrNameLength" required>
								</div>

								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
									<input type="button" class="col-xs-12 col-md-12 col-lg-12  btn cariparma-btn" value="Change" onclick="doSubmit()" />
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
				</form>
			</div>
		</div>
		<!-- /content/body -->

	</div>

	<!-- footer -->
	<footer class="navbar navbar-default navbar-fixed-bottom cariparma-footer">
		<div class="container-fluid"></div>
	</footer>

	<script src="./resources/js/jquery-2.1.4.min.js"></script>
	<script src="./resources/js/bootstrap.min.js"></script>
	<script src="./resources/js/zxcvbn.js"></script>
	<script src="./resources/js/pwstrength.js"></script>
	<script src="./resources/js/messenger.min.js"></script>

	<script type="text/javascript">
		function doSubmit() {
			var pass = document.getElementById("password").value
			var confPass = document.getElementById("confirmPassword").value
			if (!(pass||confPass)  || pass != confPass) {
				$("#alert").show(1);
				pass = null;
				confPass = null;
				document.getElementById("resetPasswordForm").reset();
				$("#password").pwstrength("forceUpdate");
			} else {
				if(pass.match(/^(?=.*\d)(?=.*[a-z])(?=.*[!@#$%&*])(?=.*[A-Z])[0-9a-zA-Z!!@#$%&*]{8,}$/)){
					$("#alert").hide(1);
					$("#alertNoPolicy").hide(1);
					/* $("#alertPolicy").hide(1); */
					 document.getElementById("resetPasswordForm").submit();
				}else{
					$("#alertNoPolicy").show(1);
					/* $("#alertPolicy").show(1); */
					showPolicy();
					document.getElementById("resetPasswordForm").reset();
					$("#password").pwstrength("forceUpdate");
				}
			}
		}
		
		
		jQuery(document).ready(function() {
			"use strict";
			var options = {};
			options.ui = {
				container : "#pwd-container",
				showVerdictsInsideProgressBar : true,
				showErrors : true,
				viewports : {
					progress : ".pwstrength_viewport_progress",
					verdict : ".messages",
					errors : ".errors"
				}
			};
			//options.common = {
				//zxcvbn : false,
				//debug : false,
				//onLoad : function() {
					/* $('#messages').text('Start typing password'); */
				//}
			//};

			options.rules = {
				activated : {
					wordNotEmail: true,
					wordLength: true,
					wordSimilarToUsername: true,
					wordSequences: true,
					wordTwoCharacterClasses: false,
					wordRepetitions: false,
					wordLowercase: true,
					wordUppercase: true,
					wordOneNumber: true,
					wordThreeNumbers: true,
					wordOneSpecialChar: true,
					wordTwoSpecialChar: true,
					wordUpperLowerCombo: true,
					wordLetterNumberCombo: true,
					wordLetterNumberCharCombo: true
				}
			};
			$('#password').pwstrength(options);
		});
		
		
		function showPolicy(){
			var msg = Messenger({extraClasses: 'messenger-fixed messenger-on-top', theme: 'air'}).post({
				message: "<strong>La password deve contenere: </strong> <ul> <li>almeno 8 caratteri</li> <li>almeno una lettera MAIUSCOLA</li> <li>almeno una lettera minuscola</li> <li>almeno un numero</li> <li>almeno un carattere speciale tra i seguenti !, @, #, $, %, &, * </li> </ul>",
				id: "Only-one-message",
				hideAfter: 20,
		  		showCloseButton: true
			});
		}
	</script>

</body>
</html>

