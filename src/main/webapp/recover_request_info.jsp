<!DOCTYPE html>
 <%
 	String action = (String) request.getAttribute("action");
 	String encodeCapchaBase64 = (String) request.getAttribute("encodeCapchaBase64");
    String captchaImageUrl = (String) request.getAttribute("captchaImageUrl");
    String captchaSecretKey = (String) request.getAttribute("captchaSecretKey");
    String captchaImagePath = (String) request.getAttribute("captchaImagePath");
    String recoveryMethod = (String) request.getAttribute("recoveryMethod");
    String validateAction = (String) request.getAttribute("validateAction");
    String callBackUrl = (String) request.getAttribute("callBackUrl");
    ServletContext ctx = config.getServletContext();

%>

    <html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>User Recover</title>

        <link rel="icon" href="./resources/images/favicon.ico" type="image/x-icon"/>
        <link href="./resources/css/bootstrap.min.css" rel="stylesheet">
        <link href="./resources/css/Roboto.css" rel="stylesheet">
        <link href="./resources/css/custom-common.css" rel="stylesheet">
        <link href="./resources/css/cariparma.css" rel="stylesheet">
        <link href="./resources/css/login.css" rel="stylesheet">

    </head>

    <body>

    <!-- header -->
   <div class="navbar navbar-inverse navbar-static-top cariparma-header">
		<div class="container">
			<a href="#" class="navbar-brand"> <img src="./resources/images/ca_logo.svg"
				alt="Control Access Module" title="Control Access Module"
				class="logo" style="height: 25px;">
			</a>
		</div>
	</div>

    <!-- page content -->    
    <div class="container-fluid body-wrapper">

        <div class="row">
            <!-- content -->
            
            <div class="container col-xs-10 col-sm-6 col-md-6 col-lg-3 col-centered wr-content wr-login col-centered">
                <form  method="POST" action="./validate" id="recoverDetailsForm">
                   <h4 class="wr-title uppercase cariparma-bg padding-double white boarder-bottom-cariparma margin-none text-align_center"><%=action%> Password</h4>

                    <div class="clearfix"></div>
                    <div class="boarder-all ">

                        <div class="padding-double font-large text-align_center">Compila i campi richiesti per il <%=action%></div>
                        <!-- validation -->
                        <div class="padding-double">
                            <div id="regFormError" class="alert alert-danger" style="display:none"></div>
                            <div id="regFormSuc" class="alert alert-success" style="display:none"></div>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group required">
                                <label class="control-label">Username</label>
                                <input id="username" name="username" type="text" class="form-control required usrName usrNameLength" required>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 form-group">
                                <label class="control-label">Captcha</label>
                                <img src="<%=encodeCapchaBase64%>" alt="Se non visualizza l'immagine captcha esegua un refresh della pagina"/>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group required">
                                <label class="control-label">Codice Captcha</label>
                                <input id="captchaAnswer" name="captchaAnswer" type="text" class="form-control required usrName usrNameLength" required>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                <button id="submit" class="col-xs-12 col-md-12 col-lg-12  btn cariparma-btn" type="submit">Conferma</button>
                            </div>
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                <button id="cancel" class="col-xs-12 col-md-12 col-lg-12  btn btn-danger" type="button"  onclick="javascript:location.href='<%=callBackUrl%>'">Cancel</button>
                            </div>
               				 <input type="hidden" name="captchaSecretKey" value="<%=captchaSecretKey%>"/>
            				 <input type="hidden" name="captchaImagePath" value="<%=captchaImagePath%>"/>
           					 <input type="hidden" name="recoveryMethod" value="<%=recoveryMethod%>"/>
           					 <input type="hidden" name="action" value="validateUserInfo"/>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /content/body -->

    </div>

   <!-- footer -->
	<footer class="navbar navbar-default navbar-fixed-bottom cariparma-footer">
		<div class="container-fluid"></div>
	</footer>

    <script src="./resources/js/jquery-2.1.4.min.js"></script>
    <script src="./resources/js/bootstrap.min.js"></script>
    </body>
    </html>

 